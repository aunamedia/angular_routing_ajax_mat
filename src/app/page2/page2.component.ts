import { Component, OnInit } from '@angular/core';
import { Observable, pipe, from, of, fromEvent, timer, interval, defer, merge, empty, range } from 'rxjs';
import { delay, map, switchMap, startWith, scan, takeWhile, mapTo } from 'rxjs/operators';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
    //emit 1-10 in sequence
    const source = range(1, 10);
    //output: 1,2,3,4,5,6,7,8,9,10
    const example = source.subscribe(val => console.log(val));
  }
}
