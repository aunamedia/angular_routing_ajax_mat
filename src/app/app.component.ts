import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  titulo:string
  constructor(private serv:AppService){
    this.titulo = this.serv.getTitulo()
  }
}

