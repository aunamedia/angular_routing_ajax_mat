import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  entorno = environment.production?"PRODUCCIÓN":"DESARROLLO"
  constructor() { }

  ngOnInit(): void {
  }

}
