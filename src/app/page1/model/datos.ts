import { Usuario } from './usuario';

export interface Datos {
    page: number,
    per_page: number,
    total: number,
    total_pages: number,
    data: Usuario[]
}
