import { Injectable } from '@angular/core';
import { ajax } from 'rxjs/ajax';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Page1Service {
  url_api: string = environment.url_api

  
  getDatos(page: number) {
    return ajax({
      url: `${this.url_api}users?page=${page}`,
      method: 'GET'
    })
  }

}
