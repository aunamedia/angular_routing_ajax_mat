import { Component, OnInit } from '@angular/core';
import { Page1Service } from './page1.service';
import { Datos } from './model/datos';
import { Usuario } from './model/usuario';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss']
})
export class Page1Component implements OnInit {
 
  visible = true;
  datos: Usuario[] = []

  ngOnInit(){
    this.cargarDatos(1)
  }

  hacerClick() {
    this.cargarDatos(1)
  }
  hacerClick2() {
    this.cargarDatos(2)
  }
  
  cargarDatos(page: number){
    this.serv.getDatos(page).subscribe(
      res => this.ponerDatos(res.response),
      err => console.log(err)
    )
  }

  ponerDatos(datos: Datos){
    this.datos = datos.data
  }
  constructor(private serv:Page1Service) {}

 
}
