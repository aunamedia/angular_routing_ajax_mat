import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Page1Component } from './page1.component';
import { MaterialModule } from '../material/material.module';

import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [Page1Component],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([
      { path: '', component: Page1Component }
    ])
  ],
  exports: [
    Page1Component,
    RouterModule
  ]
})
export class Page1Module { }



