import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  TITULO = "APP-AGC"
  constructor() { }
  getTitulo(){
    return this.TITULO;
  }
}
